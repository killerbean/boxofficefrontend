export const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Profile',
    icon: 'user',
    path: '/profile'
  },
  {
    text: 'Administrator',
    icon: 'card',
    path: '/admin'
  }
];
