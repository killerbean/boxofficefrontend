import { Agenda } from './agenda.model';

export class PagedAgenda {
    constructor(
        public lists?: Agenda[],
        public pageNumber?: number,
        public pageSize?: number,
        public nextPageNumber?: number,
        public previousPageNumber?: number
    ) { }
}
