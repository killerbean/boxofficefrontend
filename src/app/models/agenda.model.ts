import { DictionaryItem } from './dictionary-item.model';
import { Show } from './show.model';

export class Agenda {
    constructor(
        public id?: number,
        public showId?: number,
        public show?: Show,
        public dayId?: number,
        public day?: DictionaryItem,
        public timeId?: number,
        public time?: DictionaryItem,
        public agendaDate?: Date,
        public ticketsQty?: number
    ) { }
}
