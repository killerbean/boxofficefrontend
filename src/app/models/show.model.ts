export class Show {
    constructor(
        public id?: any,
        public name?: string,
        public posterUrl?: string,
        public created?: string,
        public createdBy?: Date,
        public lastModified?: string,
        public lastModifiedBy?: Date
    ) { }
}
