import { DictionaryItem } from './dictionary-item.model';

export class PagedDictionaryItem {
    constructor(
        public lists?: DictionaryItem[]
    ) { }
}
