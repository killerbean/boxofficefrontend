import { Show } from './show.model';

export class PagedShow {
    constructor(
        public lists?: Show[]
    ) { }
}
