import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxOfficeComponent } from './box-office.component';
import { BoxOfficeItemModule } from '../box-office-item/box-office-item.module';

@NgModule({
    imports: [
        CommonModule,
        BoxOfficeItemModule,
        //   DxButtonModule
    ],
    declarations: [
        BoxOfficeComponent
    ],
    exports: [BoxOfficeComponent]
})
export class BoxOfficeModule { }
