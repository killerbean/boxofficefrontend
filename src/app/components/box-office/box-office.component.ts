import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PagedAgenda } from 'src/app/models/paged.agenda.model';
import { Agenda } from 'src/app/models/agenda.model';

@Component({
  selector: 'app-box-office',
  templateUrl: './box-office.component.html',
  styleUrls: ['./box-office.component.scss']
})
export class BoxOfficeComponent implements OnInit {
  @Input() dataSource$: Observable<PagedAgenda>;
  @Output() buy: EventEmitter<Agenda> = new EventEmitter();
  agendas: Agenda[];  

  constructor() { }

  ngOnInit() {
    this.dataSource$.pipe(map((result: PagedAgenda) => {
      this.agendas = result.lists;
    })).subscribe();
  }

  onBuyItem(data: Agenda) {
    this.buy.emit(data);
  }
}
