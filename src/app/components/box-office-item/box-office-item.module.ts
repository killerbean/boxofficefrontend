import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxOfficeItemComponent } from './box-office-item.component';
import { DxButtonModule } from 'devextreme-angular';

@NgModule({
    imports: [
        CommonModule,
        DxButtonModule
    ],
    declarations: [
        BoxOfficeItemComponent
    ],
    exports: [BoxOfficeItemComponent]
})
export class BoxOfficeItemModule { }
