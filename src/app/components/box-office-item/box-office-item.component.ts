import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Agenda } from 'src/app/models/agenda.model';

@Component({
  selector: 'app-box-office-item',
  templateUrl: './box-office-item.component.html',
  styleUrls: ['./box-office-item.component.scss']
})

export class BoxOfficeItemComponent implements OnInit {
  @Input() dataSource: Agenda;
  @Output() buyItem: EventEmitter<Agenda> = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  onBuyTicket() {
    this.buyItem.emit(this.dataSource);
  }
}
