import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './components';
import { AuthGuardService } from './services';
import { ProfileComponent } from './pages/profile/profile.component';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';
import { BoxOfficeModule } from './components/box-office/box-office.module';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'home',
    loadChildren: () => import('@pages/home/home.module').then(m => m.HomeModule),
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'place-order',
    loadChildren: () => import('@pages/place-order/place-order.module').then(m => m.PlaceOrderModule),
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'admin',
    loadChildren: () => import('@pages/admin/admin.module').then(m => m.AdminModule),
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'login-form',
    component: LoginFormComponent
  },
  {
    path: '**',
    redirectTo: 'home',
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    DxDataGridModule, 
    DxFormModule,
    BoxOfficeModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
  // TODO: rewrite to lazy loading
  declarations: [ProfileComponent]
})
export class AppRoutingModule { }
