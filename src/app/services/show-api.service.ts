import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvironmentUrlService } from './environment-url.service';
import { Observable } from 'rxjs';
import { PagedShow } from '../models/paged.show.model';
import { Show } from '../models/show.model';

@Injectable({
    providedIn: 'root'
})
export class ShowApiService {

    constructor(
        private environmentUrlService: EnvironmentUrlService,
        private http: HttpClient,
    ) { }

    getShows(): Observable<PagedShow> {
        return this.http.get<PagedShow>(
            `${this.environmentUrlService.boxOfficeServiceAddress}shows`
        );
    }

    insertShow(show: Show) {
        return this.http.post<Show>(
            `${this.environmentUrlService.boxOfficeServiceAddress}shows`,
            {
                show: {
                    name: show.name,
                    posterUrl: show.posterUrl
                }
            }
        );
    }

    updateShow(show: Show) {
        return this.http.put<Show>(
            `${this.environmentUrlService.boxOfficeServiceAddress}shows/${show.id}`,
            {
                id: show.id,
                name: show.name,
                posterUrl: show.posterUrl
            }
        );
    }

    deleteShow(show: Show) {
        return this.http.delete<Show>(
            `${this.environmentUrlService.boxOfficeServiceAddress}shows/${show.id}`
        );    
    }
}
