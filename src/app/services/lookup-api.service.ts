import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EnvironmentUrlService } from './environment-url.service';
import { PagedDictionaryItem } from '../models/paged.dictionry-item.model';

@Injectable({
    providedIn: 'root'
})
export class LookupApiService {

    constructor(
        private environmentUrlService: EnvironmentUrlService,
        private http: HttpClient,
    ) { }

    getDays(): Observable<PagedDictionaryItem> {
        return this.http.get<PagedDictionaryItem>(
            `${this.environmentUrlService.boxOfficeServiceAddress}days`
        );
    }

    getTimes(): Observable<PagedDictionaryItem> {
        return this.http.get<PagedDictionaryItem>(
            `${this.environmentUrlService.boxOfficeServiceAddress}times`
        );
    }
}
