import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }

  public Information(text: string) {
    notify({
      message: text,
      position: {
        my: 'center bottom',
        at: 'center bottom'
      }
    }, 'info', 5000);
  }

  public Warning(text: string) {
    notify({
      message: text,
      position: {
        my: 'center bottom',
        at: 'center bottom'
      }
    }, 'warning', 5000);
  }

  public Error(text: string) {
    notify({
      message: text,
      position: {
        my: 'center bottom',
        at: 'center bottom'
      }
    }, 'error', 5000);
  }

  public Success(text: string) {
    notify({
      message: text,
      position: {
        my: 'center bottom',
        at: 'center bottom'
      }
    }, 'success', 5000);
  }
}
