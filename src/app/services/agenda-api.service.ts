import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvironmentUrlService } from './environment-url.service';
import { Observable } from 'rxjs';
import { PagedAgenda } from '../models/paged.agenda.model';
import { Agenda } from '../models/agenda.model';

@Injectable({
    providedIn: 'root'
})
export class AgendaApiService {

    constructor(
        private environmentUrlService: EnvironmentUrlService,
        private http: HttpClient,
    ) { }

    getAgendas(): Observable<PagedAgenda> {
        return this.http.get<PagedAgenda>(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas`
        );
    }

    getAgendasPaged(pageNumber: number, pageSize: number): Observable<PagedAgenda> {
        return this.http.get<PagedAgenda>(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas?pageNumber=${pageNumber}&pageSize=${pageSize}`
        );
    }

    sendEmail(agenda: Agenda, mail: string) {
        return this.http.post(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas/send-email`,
            { 
                agenda,
                mail
            }
        );
    }

    insertAgenda(agenda: Agenda) {
        return this.http.post<Agenda>(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas`,
            {
                agenda: {
                    showId: agenda.showId,
                    dayId: agenda.dayId,
                    timeId: agenda.timeId,
                    agendaDate: agenda.agendaDate,
                    ticketsQty: agenda.ticketsQty
                }
            }
        );
    }

    updateAgenda(agenda: Agenda) {
        return this.http.put<Agenda>(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas/${agenda.id}`,
            {
                id: agenda.id,
                showId: agenda.showId,
                dayId: agenda.dayId,
                timeId: agenda.timeId,
                agendaDate: agenda.agendaDate,
                ticketsQty: agenda.ticketsQty
            }
        );
    }

    deleteAgenda(agenda: Agenda) {
        return this.http.delete<Agenda>(
            `${this.environmentUrlService.boxOfficeServiceAddress}agendas/${agenda.id}`
        );    
    }
}
