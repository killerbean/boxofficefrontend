import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentUrlService {
  public boxOfficeServiceAddress: string = environment.boxOfficeServiceAddress;

  constructor() { }
}
