import { Component, OnInit } from '@angular/core';
import { AgendaUIQuery } from 'src/app/stores/agenda.store';
import { Observable } from 'rxjs';
import { Agenda } from 'src/app/models/agenda.model';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AgendaApiService } from 'src/app/services/agenda-api.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {
  agenda$: Observable<Agenda>;
  agenda: Agenda;
  emailValue: string;

  constructor(
    private router: Router,
    private query: AgendaUIQuery,
    private agendaApiService: AgendaApiService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.agenda$ = this.query.select('agendaToBuy');

    if (this.agenda$ === undefined) {
      return;
    }

    this.agenda$.pipe(
      map(result => {
        // If no any agenda, return Home
        if (result.id === 0) {
          this.router.navigate(['/home']);
        }

        this.agenda = result;
      })
    ).subscribe();
  }

  onSendByEmail() {
    if (this.emailValue === undefined || this.emailValue === null) {
      this.notificationService.Error('Заполните почту');
    }

    this.agendaApiService.sendEmail(this.agenda, this.emailValue)
      .subscribe(() => {
        this.router.navigate(['/home']);
      });

    this.notificationService.Success('Тут должен быть отправлен email, но у меня нет SMTP-сервера. Но на бекенде ' + 
      'отнимается -1 при оформлении билета');
  }

  onRegister() {
    console.log(this.agenda);
  }

  onGoBack() {
    this.router.navigate(['/home']);
  }
}
