import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DxButtonModule, DxTextBoxModule } from 'devextreme-angular';
import { NgModule } from '@angular/core';
import { BoxOfficeModule } from 'src/app/components/box-office/box-office.module';
import { PlaceOrderComponent } from './place-order.component';

const routes: Routes = [
    { path: '', component: PlaceOrderComponent }
  ];
  
  @NgModule({imports: [
      CommonModule,
      DxButtonModule,
      DxTextBoxModule,
      RouterModule.forChild(routes),
      BoxOfficeModule
    ],
    declarations: [ PlaceOrderComponent ],
    exports: [ PlaceOrderComponent ]
  })
  export class PlaceOrderModule {}
  