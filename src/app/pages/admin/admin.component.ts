import { Component, OnInit } from '@angular/core';
import { PagedShow } from 'src/app/models/paged.show.model';
import { Observable } from 'rxjs';
import { ShowApiService } from 'src/app/services/show-api.service';
import { Show } from 'src/app/models/show.model';
import { map } from 'rxjs/operators';
import { AgendaApiService } from 'src/app/services/agenda-api.service';
import { PagedAgenda } from 'src/app/models/paged.agenda.model';
import { Agenda } from 'src/app/models/agenda.model';
import { LookupApiService } from 'src/app/services/lookup-api.service';
import { DictionaryItem } from 'src/app/models/dictionary-item.model';
import { PagedDictionaryItem } from 'src/app/models/paged.dictionry-item.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public readonly dateFormat = 'dd/MM/yyyy';
  shows$: Observable<PagedShow>;
  shows: Show[];
  agendas$: Observable<PagedAgenda>;
  agendas: Agenda[];
  days$: Observable<PagedDictionaryItem>;
  days: DictionaryItem[];
  times$: Observable<PagedDictionaryItem>;
  times: DictionaryItem[];
  
  constructor(
    private datePipe: DatePipe,
    private showApiService: ShowApiService,
    private agendaApiService: AgendaApiService,
    private lookupApiService: LookupApiService
  ) { }

  ngOnInit() {
    this.loadLookups();
    this.loadData();    
  }

  loadData() {
    this.shows$ = this.showApiService.getShows();

    this.shows$.pipe(
      map(result => {
        this.shows = result.lists;
      })
    ).subscribe();

    this.agendas$ = this.agendaApiService.getAgendas();

    this.agendas$.pipe(
      map(result => {
        this.agendas = result.lists;
      })
    ).subscribe();
  }

  loadLookups() {
    this.days$ = this.lookupApiService.getDays();
    this.days$.pipe(
      map(result => {
        this.days = result.lists
      })
    ).subscribe();

    this.times$ = this.lookupApiService.getTimes();
    this.times$.pipe(
      map(result => {
        this.times = result.lists
      })
    ).subscribe();    
  }

  rowShowInserting(args: any) {
    this.showApiService.insertShow(args.data)
      .subscribe();
  }

  rowShowUpdated(args: any) {
    this.showApiService.updateShow(args.data)
      .subscribe();
  }

  rowShowRemoved(args: any) {
    this.showApiService.deleteShow(args.data)
      .subscribe();
  }

  rowAgendaInserting(args: any) {
    this.agendaApiService.insertAgenda(args.data)
      .subscribe();
  }

  rowAgendaUpdated(args: any) {
    this.agendaApiService.updateAgenda(args.data)
      .subscribe();
  }

  rowAgendaRemoved(args: any) {
    this.agendaApiService.deleteAgenda(args.data)
      .subscribe();
  }
}
