import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DxButtonModule, DxDataGridModule } from 'devextreme-angular';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';

const routes: Routes = [
    { path: '', component: AdminComponent }
  ];
  
  @NgModule({imports: [
      CommonModule,      
      DxButtonModule,
      DxDataGridModule,
      RouterModule.forChild(routes)
    ],
    declarations: [ AdminComponent ],
    exports: [ AdminComponent ]
  })
  export class AdminModule {}
  