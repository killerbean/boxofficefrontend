import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PagedAgenda } from 'src/app/models/paged.agenda.model';
import { AgendaApiService } from 'src/app/services/agenda-api.service';
import { Agenda } from 'src/app/models/agenda.model';
import { Router } from '@angular/router';
import { AgendaUIStore } from 'src/app/stores/agenda.store';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: [ './home.component.scss' ]
})

export class HomeComponent implements OnInit {
  agendas$: Observable<PagedAgenda>;
  subject: Subject<PagedAgenda> = new Subject();
  pageNumber: number;
  prevPageNumber: number;
  nextPageNumber: number;  
  pageSize: number;

  constructor(
    private router: Router,
    private agendaApiService: AgendaApiService,
    private store: AgendaUIStore
  ) {}

  ngOnInit() {
    this.agendas$ = this.agendaApiService.getAgendasPaged(1, 6);
    this.agendas$.pipe(
      map(result  => {
        this.pageNumber = result.pageNumber;
        this.pageSize = result.pageSize;
        this.prevPageNumber = result.previousPageNumber;
        this.nextPageNumber = result.nextPageNumber;
      })
    ).subscribe();
  }

  onBuy(data: Agenda) {
    this.store.update({ agendaToBuy: data });
    this.router.navigate(['/place-order']);
  }

  prevPage() {
    console.log(this.prevPageNumber);
    if (this.prevPageNumber === null || this.pageSize === null) {
      return;
    }

    this.agendas$ = this.agendaApiService.getAgendasPaged(this.prevPageNumber, this.pageSize);
    this.agendas$.pipe(
      map(result  => {
        this.pageNumber = result.pageNumber;
        this.pageSize = result.pageSize;
        this.prevPageNumber = result.previousPageNumber;
        this.nextPageNumber = result.nextPageNumber;
      })
    ).subscribe();
  }

  nextPage() {
    if (this.nextPageNumber === null || this.pageSize === null) {
      return;
    }

    this.agendas$ = this.agendaApiService.getAgendasPaged(this.nextPageNumber, this.pageSize);
    this.agendas$.pipe(
      map(result  => {
        this.pageNumber = result.pageNumber;
        this.pageSize = result.pageSize;
        this.prevPageNumber = result.previousPageNumber;
        this.nextPageNumber = result.nextPageNumber;
      })
    ).subscribe();
  }
}
