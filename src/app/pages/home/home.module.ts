import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { DxButtonModule } from 'devextreme-angular';
import { NgModule } from '@angular/core';
import { BoxOfficeModule } from 'src/app/components/box-office/box-office.module';

const routes: Routes = [
    { path: '', component: HomeComponent }
  ];
  
  @NgModule({imports: [
      CommonModule,
      DxButtonModule,      
      RouterModule.forChild(routes),
      BoxOfficeModule
    ],
    declarations: [ HomeComponent ],
    exports: [ HomeComponent ]
  })
  export class HomeModule {}
  