import { EntityState, EntityStore, StoreConfig, QueryEntity } from '@datorama/akita';
import { Agenda } from '../models/agenda.model';
import { Injectable } from '@angular/core';
import { DictionaryItem } from '../models/dictionary-item.model';

function createInitialState(): AgendaUIState {
    return {
        agendaToBuy: {
            id: 0,
            showId: 0,
            show: new DictionaryItem(),
            dayId: 0,
            day: new DictionaryItem(),
            timeId: 0,
            time: new DictionaryItem(),
            agendaDate: new Date(),
            ticketsQty: 0
        }
    };
  }

export interface AgendaUIState extends EntityState<Agenda> {
    agendaToBuy: Agenda
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'AgendaUIStore', idKey: 'id' })
export class AgendaUIStore extends EntityStore<AgendaUIState> {
  constructor() {
    super(createInitialState());
  }
}

@Injectable({ providedIn: 'root' })
export class AgendaUIQuery extends QueryEntity<AgendaUIState> {

    constructor(protected store: AgendaUIStore) {
        super(store);
    }
}
