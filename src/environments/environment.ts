export const environment = {
  boxOfficeServiceAddress: 'https://localhost:5001/api/',
  // boxOfficeServiceAddress: 'https://box-office-service-f4ntec.azurewebsites.net/api/',
  production: false
};
