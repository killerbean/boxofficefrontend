# BoxOfficeWebapp

## Использованы следующие технологии:
1. Angular
2. UI компоненты - DevExpress
3. Akita - реализация Redux
4. RxJS - подход для работы с объектами

Более детальная информация в репозитории с бекендом "git clone https://killerbean@bitbucket.org/killerbean/boxofficebackend.git"
